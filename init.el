;;; package --- Summary:
;;; Commentary:
;; (package-initialize)

;; Melpa
(require 'package)
(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/") t)
;;; Code:
;; Bootstrap 'use-package'
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

;;;; generic

;;"A defined abbrev is a word which expands"
(setq-default abbrev-mode t)

;; can just type y instead of yes
(fset 'yes-or-no-p 'y-or-n-p)

(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(global-display-line-numbers-mode)
(set-default 'truncate-lines t)

(global-set-key (kbd "M-z") 'zap-up-to-char)
(global-set-key (kbd "C-s") 'isearch-forward-regexp)
(global-set-key (kbd "C-r") 'isearch-backward-regexp)
(global-set-key (kbd "C-M-s") 'isearch-forward)
(global-set-key (kbd "C-M-r") 'isearch-backward)
(show-paren-mode 1)
(setq save-interprogram-paste-before-kill t
      apropos-do-all t
      mouse-yank-at-point t
      require-final-newline t
      load-prefer-newer t
      ediff-window-setup-function 'ediff-setup-windows-plain
      save-place-file (concat user-emacs-directory "places")
      backup-directory-alist `(("." . ,(concat user-emacs-directory
					       "backups"))))


;; disable minimizing frame
(global-unset-key (kbd "C-z"))
;; disable page-up and page-down
(global-unset-key (kbd "<prior>"))
(global-unset-key (kbd "<next>"))

;; pick up environment variables from .envrc files
(use-package direnv
  :config
  (direnv-mode))

;;  version control
(use-package magit
  :defer 2
  :bind ("C-c g" . magit-status))

(use-package git-timemachine
  :defer t
  :bind ("C-c C-h" . git-timemachine-toggle))

(use-package git-gutter
  :diminish git-gutter-mode
  :custom
  (git-gutter:modified-sign "~")
  (git-gutter:added-sign    "+")
  (git-gutter:deleted-sign  "-")
  :custom-face
  (git-gutter:modified ((t (:foreground "#000000" :background "#87cefa"))))
  (git-gutter:added    ((t (:foreground "#000000" :background "#50fa7b"))))
  (git-gutter:deleted  ((t (:foreground "#000000" :background "#ff79c6"))))
  :config
  (global-git-gutter-mode +1)
  )


;; hit this to fix whitespace, nice to use use together with M-^
(define-key global-map "\C-c " 'fixup-whitespace)

;; (use-package zone
;;   :config (zone-when-idle 1028))

;;;; visual / looks

;; make window a little bit transparent
;;(set-frame-parameter (selected-frame) 'alpha '95)
;; (add-to-list 'default-frame-alist '(alpha '95))

(use-package doom-themes
  :ensure t
  :config (setq inhibit-startup-screen t)
  (setq custom-safe-themes t)
  ;; (load-theme 'doom-solarized-light)
  (load-theme 'doom-laserwave)
  ;;(set-default-font "Hasklig")
  ;; https://fontlibrary.org/en/font/fantasque-sans-mono
  ;; (set-frame-font "Fantasque Sans Mono")
  ;;(set-frame-font "Purisa Mono")
  )

;; always use dark theme in terminal mode
(if (not(display-graphic-p))
    (load-theme 'doom-laserwave))

(use-package beacon
  :defer 2
  :custom
  (beacon-color "#f1fa8c")
  :hook (after-init . beacon-mode)
  :bind  ("C-<tab>" . 'beacon-blink))

;; mouse-wheel Ctrl-scroll zoom
(global-set-key [C-mouse-4] 'text-scale-increase)
(global-set-key [C-mouse-5] 'text-scale-decrease)

;; Scroll less than default
(defvar fk/default-scroll-lines 5)

(defun fk/scroll-up (orig-func &optional arg)
  "Scroll up `fk/default-scroll-lines' lines (probably less than default)."
  (apply orig-func (list (or arg fk/default-scroll-lines))))

(defun fk/scroll-down (orig-func &optional arg)
  "Scroll down `fk/default-scroll-lines' lines (probably less than default)."
  (apply orig-func (list (or arg fk/default-scroll-lines))))

(advice-add 'scroll-up :around 'fk/scroll-up)
(advice-add 'scroll-down :around 'fk/scroll-down)


(setq ring-bell-function 'ignore)

;; less clutter in mode-line
(use-package diminish
  :defer t
  )

;;;; editing
(use-package multiple-cursors
  :defer 2
  :config
  (global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
  (global-set-key (kbd "C->") 'mc/mark-next-like-this)
  (global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
  (global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this))

;; no tabs (use C-q when must use tabs)
(setq-default indent-tabs-mode nil)

(setq show-trailing-whitespace nil)

(defun tf-toggle-show-trailing-whitespace ()
  "Toggle show-trailing-whitespace between t and nil."
  (interactive)
  (setq show-trailing-whitespace (not show-trailing-whitespace)))

(global-set-key (kbd "C-c <deletechar>") 'delete-trailing-whitespace)

;;;; navigation

;; modeline shows name of the function you are in
(which-function-mode t)

;;(setq scroll-lock-mode t)
;; (global-hl-line-mode)
(setq blink-cursor-mode nil)
;; swap easily between vertical/horizontal arrangement
(use-package transpose-frame
  :defer 2
  :config (global-set-key (kbd "C-|") 'transpose-frame))

(use-package zygospore
  :defer 2
  :config (global-set-key (kbd "C-x 1") 'zygospore-toggle-delete-other-windows))

(use-package projectile
  :diminish projectile-mode
  :config (projectile-mode +1)
  )

(global-set-key (kbd "<f5>")
                (lambda () (interactive)
                  (find-file "~/code/notes")
                  (projectile-find-file)))


;; better running commands by name execute, (M-x), fuzzy completion included
(global-set-key (kbd "M-x") 'helm-M-x)

(use-package rg)
(use-package helm-rg)

(use-package helm-projectile
  :defer 1
  :bind
  ("C-x x" . helm-buffers-list)
  ("C-c f" . helm-projectile)
  ("C-c s" . helm-projectile-rg)
  ("C-c i" . helm-imenu)
  ("C-c p" . helm-projectile-switch-project))

(setq projectile-enable-caching t)
(setq projectile-indexing-method 'alien)
(setq projectile-globally-ignored-directories '(".git"))
(setq projectile-globally-ignored-directories '("node_modules"))
(setq projectile-globally-ignored-directories '(".stack-work"))

(use-package expand-region
  :defer t
  :bind ("C-=" . er/expand-region))

(use-package wrap-region
  :config
  (wrap-region-add-wrappers
   '(("*" "*" nil org-mode)
     ("~" "~" nil org-mode)
     ("/" "/" nil org-mode)
     ("=" "=" nil org-mode)
     ("+" "+" nil org-mode)
     ("_" "_" nil org-mode)
     ("$" "$" nil (org-mode latex-mode))))
  (add-hook 'org-mode-hook 'wrap-region-mode)
  (add-hook 'latex-mode-hook 'wrap-region-mode))

(use-package flx-ido
  :ensure t
  :requires ido
  :config (flx-ido-mode))

(use-package avy
  :ensure t
  :commands (avy-goto-char-timer)
  :config
  (setq avy-timeout-seconds 0.25)
  :bind (("<C-return>" . avy-goto-char-timer)
         ("C-M-'" . avy-goto-char-timer)
	 ("<C-M-return>" . avy-goto-line)))

(use-package ivy
  :defer t
  )

(use-package which-key
  :defer 2
  :diminish which-key-mode
  :config (which-key-mode)
  (setq which-key-idle-delay 0.1))

;; move around with shift+arrowkeys
;;(windmove-default-keybindings)


;;;; autocompletion
(use-package company
  :defer 1
  :diminish company-mode
  :bind ("TAB" . company-indent-or-complete-common)
  (:map company-active-map
        ("C-n" . company-select-next)
        ("C-p" . company-select-previous)
        ("<tab>" . company-complete-common-or-cycle)
        :map company-search-map
        ("C-p" . company-select-previous)
        ("C-n" . company-select-next))
  :custom
  (company-idle-delay 0)
  (company-echo-delay 0)
  (company-minimum-prefix-length 2)
  :config
  ;; invert the navigation direction if the the completion popup-isearch-match
  ;; is displayed on top (happens near the bottom of windows)
  (setq company-tooltip-flip-when-above t)
  (global-company-mode)
  )

;; (use-package company-posframe
;;   :defer t
;;   :config
;;   (company-posframe-mode 1)
;;   )

;; (use-package company-ghci
;;   :defer t
;;   :hook (haskell-mode . company-mode)
;;   :config (setq company-idle-delay 0.05
;; 		company-minimum-prefix-length 2))

;; (use-package company-ghc
;;   :defer t
;;   :hook (haskell-mode . company-mode)
;;   :config (setq company-idle-delay 0.05
;; 		company-minimum-prefix-length 2))


;;;; syntax checking
(use-package flycheck
  :defer t
  :diminish flycheck-mode
  :config
  (add-hook 'after-init-hook #'global-flycheck-mode)
  (setq flycheck-display-errors-function #'flycheck-display-error-messages-unless-error-list)
  (set-face-underline 'flycheck-warning nil)
  :custom
  (flycheck-display-errors-delay 0)
  :bind
  (("C-c C-e" . list-flycheck-errors)
   ("C-c C-n" . flycheck-next-error)
   ("C-c n" . flycheck-next-error)
   ("C-c C-p" . flycheck-previous-error)
   ("M-n" . flycheck-next-error)
   ("M-p" . flycheck-previous-error))
  )

;; (use-package flycheck-posframe
;;   :hook (flycheck-mode . flycheck-posframe-mode)
;;   :config
;;   (flycheck-posframe-configure-pretty-defaults)
;;   (setq flycheck-posframe-border-width 30))

;; Nope, I want my copies in the system temp dir.
(setq flymake-run-in-place nil)
;; Fix for hot reloaders freaking out over .#Files "[Error: ENOENT: no such file or directory"
(setq create-lockfiles nil)
;; This lets me say where my temp dir is. (make sure it exists)
(setq temporary-file-directory "~/.emacs.d/tmp")

;; (add-to-list 'load-path "~/.emacs.d/flycheck-inline")
;; (require 'flycheck-inline)

;;;; clojure
;; (use-package rainbow-delimiters
;;   :defer t
;;   :hook (prog-mode . rainbow-delimiters-mode))

;; (use-package clojure-mode
;;   :defer t
;;   :config
;;   (add-hook 'clojure-mode-hook 'paredit-mode)
;;   (add-hook 'clojure-mode-hook 'rainbow-delimiters-mode))

;; (use-package paredit
;;   :defer 4
;;   :config
;;   (add-hook 'clojure-mode-hook 'enable-paredit-mode)
;;   (add-hook 'clojure-repl-mode-hook 'enable-paredit-mode)
;;   (add-hook 'emacs-lisp-mode-hook 'enable-paredit-mode))

;; (defun my-clojure-mode-hook ()
;;   "Initialize clojure refactoring and code snippets."
;;   (yas-minor-mode 1) ;;for adding require/use/import statements
;;   )

;; (global-set-key (kbd "C-c t") #'transpose-sexps)

;; (use-package cider
;;   :defer t
;;   :init (add-hook 'clojure-mode-hook #'cider-mode)
;;   :config
;;   (setq nrepl-log-messages t)
;;   (add-hook 'cider-mode-hook #'cider-company-enable-fuzzy-completion)
;;   (add-hook 'cider-repl-mode-hook #'cider-company-enable-fuzzy-completion)
;;   (add-hook 'cider-repl-mode-hook #'paredit-mode)
;;   (add-hook 'cider-repl-mode-hook #'rainbow-delimiters-mode))

;; (use-package aggressive-indent
;;   :defer t
;;   :init (add-hook 'clojure-mode-hook #'aggressive-indent-mode))


;;;; rust
(use-package toml-mode)

(use-package rustic
  :ensure t
  :custom
  ;; (lsp-rust-analyzer-server-display-inlay-hints t) ;; types everywhere
  (lsp-rust-analyzer-cargo-watch-command "clippy")
  (lsp-rust-analyzer-display-lifetime-elision-hints-enable "skip_trivial")
  (lsp-rust-analyzer-display-chaining-hints t)
  (lsp-rust-analyzer-display-lifetime-elision-hints-use-parameter-names nil)
  (lsp-rust-analyzer-display-closure-return-type-hints t)
  (lsp-rust-analyzer-display-parameter-hints nil)
  (lsp-rust-analyzer-display-reborrow-hints nil)
  :config
  (lsp-toggle-signature-auto-activate)
  ;; (require 'dap-cpptools)
  :hook (
         (rustic-mode . subword-mode)
         (rustic-mode . yas-minor-mode))
  )

(use-package cargo
  :defer t
  :hook (rust-mode . cargo-minor-mode))

(use-package flycheck-rust
  :defer t
  :config (add-hook 'flycheck-mode-hook #'flycheck-rust-setup))

;;;; haskell
;; (use-package haskell-mode
;;   :config
;;   (add-to-list 'auto-mode-alist '("\\.hs\\'" . haskell-mode))
;;   (add-hook 'haskell-mode-hook 'subword-mode)
;;   (add-hook 'haskell-mode-hook #'yas-minor-mode))

;; (use-package flycheck-haskell
;;   :config (add-hook 'haskell-mode-hook #'flycheck-haskell-setup))

;; nixos

;; (use-package nix-mode
;;   :defer 2
;; )

;; elm

;; (use-package elm-mode
;;   :hook ((elm-mode . lsp-deferred)
;;          (elm-mode . subword-mode)))

;;;; lsp
(use-package lsp-mode
  :defer 2
  :ensure t
  :commands lsp
  :config
  (setq lsp-prefer-capf t)
  (setq lsp-prefer-flymake nil)
  '(lsp-lsp-flycheck-warning-unnecessary-face ((t (:underline "DarkOrange1"))) t)
  (setq lsp-idle-delay 0.500)

  :commands (lsp-deferred))


;; (use-package lsp-haskell
;;   :hook (haskell-mode . lsp-deferred)
;;   :config
;;   (setq lsp-haskell-process-path-hie "haskell-language-server-wrapper")
;;   )

                                        ;(use-package company-lsp :commands company-lsp)
(use-package helm-lsp :commands helm-lsp-workspace-symbol)
(use-package yasnippet)

;; (global-set-key [f8] 'treemacs)		;
;; (use-package treemacs
;;   :defer t
;;   )
;; (use-package treemacs-projectile)
;; (use-package lsp-treemacs
;;   :defer t
;;   ;; :bind
;;   ;; ("C-c C-e" . lsp-treemacs-errors-list)
;;   )

(use-package lsp-ui
  :custom
  ;; lsp-ui-doc
  (lsp-ui-doc-enable nil)
  (lsp-ui-doc-header t)
  (lsp-ui-doc-include-signature nil)
  (lsp-ui-doc-position 'at-point)
  (lsp-ui-doc-max-width 120)
  (lsp-ui-doc-max-height 30)
  (lsp-ui-doc-use-childframe t)
  ;; lsp-ui-sideline
  (lsp-ui-sideline-ignore-duplicate t)
  (lsp-ui-sideline-show-symbol nil)
  (lsp-ui-sideline-show-hover nil)
  (lsp-ui-sideline-show-diagnostics nil)
  (lsp-ui-sideline-show-code-actions t)
  ;; lsp-ui-imenu
  (lsp-ui-imenu-enable t)
  (lsp-ui-imenu-kind-position 'top)
  ;; lsp-ui-peek
  (lsp-ui-peek-enable t)
  (lsp-ui-peek-show-directory t)
  (lsp-ui-peek-peek-height 20)
  (lsp-ui-peek-list-width 50)
  (lsp-ui-peek-fontify 'always)
  :bind
  (:map lsp-mode-map
	("C-c C-t" . lsp-describe-thing-at-point)
	("C-c C-r" . lsp-ui-peek-find-references)
	("C-c C-j" . lsp-ui-peek-find-definitions)
	("C-c C-j" . lsp-ui-peek-find-implementation)
	("C-c C-i" . lsp-ui-peek-find-implementation)
        ("C-c C-l C-r" . lsp-rename)
        ("C-c C-l C-f" . rustic-format-buffer)
        ("C-c C-l C-n" . git-gutter:next-hunk)
        ("C-c C-l C-p" . git-gutter:previous-hunk)
	("C-c C-m" . lsp-ui-imenu)
	("C-c C-s" . lsp-ui-sideline-mode)
	("M-RET"   . helm-lsp-code-actions)
	("C-c C-d" . lsp-ui-doc-mode)))

;;;; javascript

(use-package json-mode
  :defer t
  )

(use-package typescript-mode
    :mode "\\.ts\\'" "\\.tsx\\'"
    :hook ((typescript-mode . lsp)
           (typescript-mode . subword-mode)))

(use-package yaml-mode
  :defer t
  :config (add-to-list 'auto-mode-alist '("\\.yml\\'" . yaml-mode)))


;;;; docs / org-mode / Org-mode

;; letters as ordered list bullets
;; A. like
;; B. this
(setq org-list-allow-alphabetical t)

(setf org-blank-before-new-entry '((heading . nil) (plain-list-item . nil)))

(setq org-hide-emphasis-markers t)

(setq org-startup-with-inline-images t)

(setq org-image-actual-width nil)

(setq org-tags-column 75)

(setq org-startup-indented t)

(use-package valign
  :defer t
  :hook (org-mode . valign-mode)
  )

;; org-agenda

(define-key global-map (kbd "C-c l") 'org-store-link)

;; attach (drag) images from web/filesystem directly to org files
;; (use-package org-download)

(use-package org-cliplink
  :defer t
  :bind ("C-x p l" . 'org-cliplink)
  )

(use-package visual-fill-column
  :defer t
  )

(defun my-nov-font-setup ()
  (face-remap-add-relative 'variable-pitch :family "Liberation Serif"
                           :height 1.0))

(add-hook 'nov-mode-hook 'my-nov-font-setup)

(use-package nov
  :defer t
  :config
  (add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode))
  (setq nov-text-width 120)
  (setq nov-text-width t)
  (setq visual-fill-column-center-text t)
  (add-hook 'nov-mode-hook 'visual-line-mode)
  (add-hook 'nov-mode-hook 'visual-fill-column-mode)
  ;; (setq line-spacing 0.0)
  )

;;;; org-babel

(use-package ob-rust :defer 2)
(use-package ob-restclient :defer 2)

(org-babel-do-load-languages
 'org-babel-load-languages
 '((gnuplot . t)
   (C . t)
   (shell . t)
   (haskell . t)
   (rust . t)
   (restclient . t)
   (dot . t)
   (java . t))
 )

;; plotting formulas
(use-package gnuplot :defer t)
(use-package gnuplot-mode :defer t)

(use-package org-bullets
  :config
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))

;; recipes
(use-package org-chef)

;; graphviz
(use-package graphviz-dot-mode)

;; tex
(use-package tex
  ;; apt install texlive-latex-extra
  :defer t
  :ensure auctex
  :config
  (setq TeX-auto-save t)
  (setq org-format-latex-options (plist-put org-format-latex-options :scale 3.5))
  )

(use-package google-translate
  :ensure t
  :custom
  (google-translate-backend-method 'curl)
  :config
  (setq google-translate-translation-directions-alist
        '(("ja" . "en") ("en" . "ja")))
  (setq google-translate-show-phonetic t)
  (progn
    ;; To fix error: google-translate--search-tkk: Search failed: ",tkk:'"
    ;; https://github.com/atykhonov/google-translate/issues/52#issuecomment-727920888
    (defun google-translate--search-tkk () "Search TKK." (list 430675 2721866130))
    (defalias 'gt 'google-translate-smooth-translate)
    ))



;; custom functions fns

(defun jisho-org-link (word tooltip)
  "Generate org-mode jisho link from word and insert macro for tooltip in html export."
  (interactive "MWord:\nMTooltip:")
  (let ((jisho-url "https://jisho.org/search/"))
    (progn
      (insert (concat "{{{kanji(" tooltip ")}}}"))
      (newline-and-indent)
      (org-insert-link t (concat jisho-url word) word)
      )))
(global-set-key (kbd "C-x C-j") 'jisho-org-link)


(defun jisho-org-link-region (start end)
  "Generate org-mode jisho search link from region."
  (interactive "r")
  (let ((query (buffer-substring start end))
        (jisho-url "https://jisho.org/search/"))
    (progn
      (kill-region start end)
      (org-insert-link nil (concat jisho-url query) query)
      )))

(defun find-user-init-file ()
  "Edit the `user-init-file', in another window."
  (interactive)
  (find-file-other-window user-init-file))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ispell-program-name "/usr/bin/enchant-2")
 '(package-selected-packages
   '(slint-mode caser valign rainbow-mode typescript-mode google-translate auctex graphviz-dot-mode org-chef org-bullets gnuplot-mode gnuplot ob-restclient nov json-mode lsp-ui helm-lsp lsp-mode flycheck-rust cargo rustic flycheck ido-completing-read+ wrap-region helm-projectile helm-rg rg git-timemachine magit direnv zygospore yasnippet yaml-mode xterm-color which-key wgrep visual-fill-column transpose-frame toml-mode spinner restclient rainbow-delimiters projectile popup org-cliplink ob-rust multiple-cursors memoize markdown-mode lv kv json-snatcher ivy git-gutter flx-ido expand-region epl doom-themes diminish dash compat company beacon avy))
 '(safe-local-variable-values '((org-image-actual-width)))
 '(typescript-indent-level 2))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(put 'narrow-to-region 'disabled nil)
(put 'narrow-to-page 'disabled nil)
